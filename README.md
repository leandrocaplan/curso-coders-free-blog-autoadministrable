Playlist:
https://www.youtube.com/watch?v=8dgNq4FbEBg&list=PLZ2ovOgdI-kX3XFj77zlvSQYhJyJSYQWr
https://codersfree.com/cursos/aprende-a-crear-un-blog-autoadministrable-con-laravel



INTRODUCCIÓN


1. Aprende a crear un blog autoadministrable con Laravel 10 (Presentación del curso)

Objetivo: Tocar temas que no vimos en curso anterior:

-Uso de Policies
-Uso de observers
-Creacion de sistema de roles y permisos
-Subir imágenes a servidor
-Trabajar con select/checkbox
-Etc.. (hay mas temas en cuestión)

Usamos plantilla (template) AdminLTE para la UI

Tenemos posts, categorias y etiquetas.

Los post se pueden filtrar, o bien por categorías, o bien por etiquetas

Diseñamos sistema de roles y permisos

Por ejemplo, solo los usuarios con ciertos permisos podrían acceder al Dashboard

Usamos el sistema de roles y permisos Laravel Spatie:
https://github.com/spatie/laravel-permission





2. Diseñar la bbdd de un blog

Un usuario va a poder subir varios posts. Un post puede ser subido por un solo usuario -> Relación 1 a n

Dentro de una categoría pueden existir varios posts. Un post solo puede pertenecer a una categoría -> Relación 1 a n

Un post puede tener varias etiquetas. Una etiqueta puede estar en varios post. -> Entre post y tags tengo Relación n a n
Necesitamos allí una tabla intermedia

Tabla 'images':
Almacenamos la URL de todas las imágenes que subamos.
Para relacionarlo con una determinada entidad, utilizamos relaciones polimórficas:
Campos: 'id','url','imageable_id','imageable_type'

Ver imágen:
diseño_bbdd_blog.png


Nos conviene crear un nuevo proyecto de ejemplo.
En la terminal, hacemos:

    leandro@leandro-Lenovo-B50-10:/var/www/html$ laravel new blog --jet

      
              |     |         |
              |,---.|--- ,---.|--- ,---.,---.,---.,-.-.
              ||---'|    `---.|    |    |---',---|| | |
          `---'`---'`---'`---'`---'`    `---'`---^` ' '


        Which Jetstream stack do you prefer?
          [0] livewire
          [1] inertia
         > 0

         Will your application use teams? (yes/no) [no]:
         > no

        Creating a "laravel/laravel" project at "./blog"
        
        (Esperamos a que descargue e instale las dependencias)
        ...
        
        INFO  Livewire scaffolding installed successfully.  

        INFO  Application ready! Build something amazing.


    leandro@leandro-Lenovo-B50-10:/var/www/html$  cd blog
    
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ npm install
        (...)


Abrimos el nuevo proyecto en VSCode (nos conviene cerrar las carpetas de otros proyectos)

Vamos a nuestro .env (/var/www/html/blog/.env)
Tenemos allí:
'DB_DATABASE=blog' -> Creamos la base de datos (se podría tambien crear automáticamente al hacer migrate)

Debemos también asegurarnos de tener:
'DB_HOST=127.0.0.1' (para evitar errores en el migrate)


En nuestro caso, también debemos asegurarnos de tener:
'DB_USERNAME=root'
'DB_PASSWORD=root'

(O bien las credenciales habilitadas que correspondan)

Luego, hacemos 'php artisan migrate' (con solamente las migrations que ya vienen por defecto cuando generamos el proyecto)

Finalmente, damos 'npm run dev'.


A continuación, procedemos a crear las migrations de las tablas que necesitamos, comenzando por las correspondientes a entidades fuertes:

    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:model Category -m
       INFO  Model [app/Models/Category.php] created successfully.  
       INFO  Migration [database/migrations/2023_10_16_002302_create_categories_table.php] created successfully.  

    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:model Post -m 
       INFO  Model [app/Models/Post.php] created successfully.  
       INFO  Migration [database/migrations/2023_10_16_002517_create_posts_table.php] created successfully.  

    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:model Tag -m
       INFO  Model [app/Models/Tag.php] created successfully.  
       INFO  Migration [database/migrations/2023_10_16_003030_create_tags_table.php] created successfully.  

Llenamos los archivos de migration generados con los campos correspondientes (ver contenido en dichos archivos, copiado del video)

Campo "status" en entidad Post:
De tipo 'enum': Vale 1 si el post se encuentra en estado borrador, y 2 si se encuentra publicado (solo dos estados posibles)

$table->enum('status',[1,2])->default(1); //POr defecto, está como borrador hasta que lo publique


Para implementar la relación 'n a n' entre post y tags, debemos generar la tabla intermedia:

    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:migration create_post_tag_table
       INFO  Migration [database/migrations/2023_10_16_004103_create_post_tag_table.php] created successfully.

Genero las FK correspondientes (ver archivo de migration, con el código copiado del video).


Solo nos queda:

    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:model Image -m
       INFO  Model [app/Models/Image.php] created successfully.  
       INFO  Migration [database/migrations/2023_10_16_004433_create_images_table.php] created successfully.  


Damos de nuevo 'php artisan migrate' para crear las tablas nuevas.


Nos queda implementar las relaciones a nivel de modelos.
Ver código copiado del video en:
'/var/www/html/blog/app/Models'





3. Cómo llenar con datos falsos la bbdd de un blog

Podemos saltearnos esta parte por ahora, ya que no es tan relevante para nosotros en este momento.
Tenemos ese tema explicado en el curso "Laravel desde cero (apuntes_laravel.txt)"

Ver carpeta:
'/var/www/html/blog/database/factories'

Ver archivo:
'/var/www/html/blog/database/seeders/DatabaseSeeder.php' (en el video está mas modularizado)

Sin embargo, podría sernos de utilidad copiar el código mostrado en el video a efectos de agilizar las pruebas.

Nos puede interesar también, desde donde descarga automáticamente las imágenes para llenar como datos de prueba.

Sí es relevante en este video:

Acceder a '<root>\config\filesytem.php' y reemplazar:

    'default' => env('FILESYSTEM_DISK', 'local'),

Por:
    'default' => env('FILESYSTEM_DISK', 'public'),

En nuestro .env debemos tener:
    'FILESYSTEM_DISK=public'

Logramos que todas las imágenes que se suban, no se almacenen en:
'...\blog\storage\app' 

Sino en:
'...\blog\storage\app\public'

(Rutas de nuestro disco, no de la estructura de URLs en el enrutamiento de la app)

Lo hacemos para que, mediante lo definido en el .gitignore, las imágenes que subamos NO se suban al repo ni a nuestro servidor local


En el .env, debemos asegurarnos de tener (por ahora):

    APP_URL=http://localhost:8000

O alguna URL definida para Apache en su defecto, mediante VirtualHost. Repasar video (en minuto 15) para ver la implementación de los mismos cuando sea necesario.






4. Como crear un menú responsive con Tailwind y Alpine
5. Como volver funcional el menú de Tailwind

Explicado en otro documento: "apuntes_tailwind.txt", Clase 14 y 15.

Para ver implementacion, ver archivos:

    /var/www/html/blog/app/Livewire/Navigation.php
    /var/www/html/blog/resources/views/livewire/navigation.blade.php
    /var/www/html/blog/resources/views/layouts/app.blade.php
    /var/www/html/blog/resources/views/welcome.blade.php

Para insertar datos de prueba en "categories" (Si no lo hicimos con seeders y factories como en los videos), podemos hacer en nuestra base de datos:

    INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES ('1', 'Casas', 'casas', NULL, NULL), ('2', 'Hoteles', 'hoteles', NULL, NULL), ('3', 'Playas', 'playas', NULL, NULL), ('4', 'Parques', 'parques', NULL, NULL);

(Podríamos incluir los timestamps)




FRONTEND DE LA APLICACIÓN



6. Cómo mostrar el listado de posts de un blog

Creamos PostController, y las vistas correspondientes.
Por ahora, sólo el método y la vista 'index', que le pasamos los post publicados:

      $posts=Post::where('status',2)->get();
      return view('posts.index',compact('posts'));

Debemos lograr que todo el contenido que se muestra en la vista permanezca centrado.

Clase "container" de Tailwind -> Hace que el tamaño del <div> varíe de acuerdo al tamaño de la pantalla

En index.blade.php tenemos:

    <x-app-layout>
       <div class="container bg-red-500">
          Pepe
       </div>
    </x-app-layout>

El <div> No está centrado -> Necesitamos que esté centrado respecto de la barra de navegación.

El 'navigation.blade.php' NO utiliza la clase 'container' para mantener centrado su contenido.

Tenemos allí:

    <div class="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8"> (Abajo del <nav> padre)

Deberíamos aplicar esas mismas clases en el <div> principal de 'index.blade.php' de 'posts':


    <x-app-layout>
       <div class="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8 bg-red-500">
          Pepe
       </div>
    </x-app-layout>

Vemos que ahora está siempre centrado respecto a la navbar.

Para evitar siempre ese código redundante:
Deshabilitamos la clase 'container' que viene por defecto en Tailwind.

Vamos a 'tailwind.config.js'.
Añadimos en el primer nivel de 'module.exports':

    corePlugins:{
      container:false,
    }

Necesitamos crear nuestra propia clase container personalizada.

Vamos a 'resources/css' y creamos el archivo 'common.css':

    .container{
       @apply mx-auto max-w-7xl px-2;
    }

    @media(min-width:640px){
       .container{
          @apply px-6;
       }
    }

    @media(min-width:1024px){
       .container{
          @apply px-8;
       }
    }

En 'app.css' hacemos:

    /*¡OJO! El @import SIEMPRE debe ir arriba de las otras directivas*/
    @import 'common.css';

Ya tenemos nuestra clase 'container' personalizada.


Abrimos un <div> con una grid en nuestro 'index':

    <x-app-layout>
       <div class="container">
          <div class="grid grid-cols-3 gap-6">
             @foreach ($posts as $post)
                <article>
                   <img src="{{Storage::url($post->image->url)}}" alt="">
                </article>
             @endforeach
          </div>
       </div>
    </x-app-layout>

Queremos mostrar un texto sobre la imagen asociada a cada post, no solo la imagen:
El <img> lo sacamos.

Definimos la siguiente estructura:

    <x-app-layout>
       <div class="container py-8">
          <div class="grid grid-cols-3 gap-6">
             @foreach ($posts as $post)
             
                <article 
                   class="w-full h-80 bg-cover bg-center
                         @if($loop->first)
                            col-span-2
                         @endif"
                   style="background-image: url({{Storage::url($post->image->url)}})">
                   

                   <div class="w-full h-full px-8 flex flex-col justify-center">
                      <div>
                         @foreach ($post->tags as $tag)
                            <a href="" class="inline-block px-3 h-6 bg-gray-600 text-white rounded-full">
                               {{$tag->name}}
                            </a>
                         @endforeach
                      </div>   
                                 
                      <h1 class="text-4xl text-white leading-8 font-bold">
                         <a href="">
                            {{$post->name}}
                         </a>
                      </h1>
                   </div>
                </article>
             @endforeach
          </div>
       </div>
    </x-app-layout>


Primer <div>:
py-8: Espaciado respecto a la navbar de toda la grilla

<article>:
bg-cover: Para que no se deforme la imágen
bg-center: Para que la imagen esté centrada
col-span-2: La imágen del primer post ocupa 2 columnas


Necesito que al hacer click en una imágen de la grilla, nos redireccione a ese post específico.
Encima de la imágen, debemos imprimir las etiquetas del post.

Comenzamos por el nombre del post.

Segundo <div> anidado::
Para que todo el contenido esté centrado: le damos un display 'flex', para que distribuya el contenido a lo largo del eje 'y', le damos 'flex-col', y para que todo el contenido esté centrado, le damos 'justify-center'.

Links <a> de las tags: tienen 'inline-block': Ocupan el menor espacio disponible (como inline), pero se pueden aplicarles margin y padding (como block).

En <h1>: 'leading-8' -> mientras mas alto el número, mas espaciado entre renglones del texto (interlineado).



Ahora, queremos que cada Tag tenga un color distinto-> Debemos ir a la migration de 'tags'
Añadimos un campo 'color' de tipo 'string'.

También editamos la factory añadiendo:
    'color' => $this->faker->randomElement(['red','yellow','green','blue','indigo','purple','pink'])

Hacemos entonces, en la vista Blade:

    <a href="" class="inline-block px-3 h-6 bg-{{$tag->color}}-600 text-white rounded-full"> (Tiene un bug, pendiente de revision)


¿Que pasa si quiero publicar solo cierta cantidad de registros?
Ej: Quiero obtener solo 8 posts, ordenados de manera ascendente (el último registro que aparezca primero

Voy al 'index' del Controller:

Reemplazo:
    $posts=Post::where('status',2)->get();

Por:
    $posts=Post::where('status',2)->latest('id')->paginate(8);

Añadimos (antes de cerrar el <div> container) para navegar entre las páginas:

  <div class="mt-4">
     {{$posts->links()}}
  </div>


Por último, queremos hacer un diseño responsivo:
Una grid de una sola columna cuando la pantalla sea muy pequeña, 2 en una mediana y 3 en una grande:

    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">

Para la primera imágen (que ocupaba 2 columnas);
    
     @if($loop->first)
        md:col-span-2
     @endif

Archivos relevantes para esta clase (ver bug en index.blade.php, se menciona en los comentarios del video de la clase):

    /var/www/html/blog/app/Http/Controllers/PostController.php
    /var/www/html/blog/resources/views/posts/index.blade.php
    /var/www/html/blog/database/migrations/2023_10_16_003030_create_tags_table.php
    /var/www/html/blog/database/factories/TagFactory.php
    /var/www/html/blog/resources/views/livewire/navigation.blade.php
    /var/www/html/blog/tailwind.config.js
    /var/www/html/blog/resources/css/common.css
    /var/www/html/blog/resources/css/app.css




7. Cómo mostrar el detalle de un post 

Creamos la vista, el método del controller, y la ruta para el 'show' de 'post'.
Debemos poder dirigirnos a ella al clickear en el texto del nombre (name) del post, mostrado en 'index'

Archivos relevantes:

    /var/www/html/blog/resources/views/posts/show.blade.php
    /var/www/html/blog/resources/views/posts/index.blade.php (Ahora tiene links a la vista 'show')
    /var/www/html/blog/app/Http/Controllers/PostController.php (Ahora tiene el método 'show')
    /var/www/html/blog/routes/web.php (Ahora tiene la ruta para el 'show')





8. Filtrar los posts de un blog por categoría

Crear ruta:
    Route::get('/category/{category}', [PostController::class,'category'])->name('posts.category');

En PostController creo un método:
        use App\Models\Category;
        ...    
       public function category(Category $category){
        $posts=Post::where('category_id',$category->id)
         ->where('status',2)
         ->latest('id')
         ->paginate(4);

      return view('posts.category',compact('posts','category'));
   }

Tenemos que ir al componente 'Navigation' de Livewire:
Buscamos los enlaces (elementos de la navbar) correspondientes a las categorías mostradas en ella.

En los links correspondientes, agregamos la nueva ruta definida al 'href' (previamente vacío), que estan dentro de los foreach allí definidos

Ver vista:
    /var/www/html/blog/resources/views/posts/category.blade.php





9. Filtrar los posts de un blog por etiquetas

Debemos crear un componente Blade, para reutilizar el código implementado en la clase anterior (ya que ambas vistas serían muy similares).

En el video, ejemplifica con un componente anónimo, pero podríamos crear un componente de clase.

Hacemos:
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:component CardPost

Nos crea:
    'app/View/Components/CardPost.php'
    'resources/views/components/card-post.blade.php'


En CardPost.php (clase del componente), debemos tener:

    class CardPost extends Component
    {
        public $post;
        public function __construct($post) {
            $this->post=$post;
        }

        public function render(): View|Closure|string {
            return view('components.card-post');
        }
    }

En 'category.blade.php', cortamos y pegamos todo lo que teníamos en <article class...>... </article> en 'card-post.blade.php'

Lo reemplazamos por una sola línea:
    <x-card-post :post="$post"/>

Nuestro resultado sigue siendo idéntico al anterior, pero mas modularizado.

Luego, creamos la rúta, el método de controller, y la vista, necesarios para filtrar por tags en lugar de por categorías
Todas serán muy similares a las definidas en la clase anterior, reemplazando 'category' por 'tag' en todos los casos.

La vista 'tag.blade.php' será casi idéntica a 'category.blade.php', únicamente modificando la línea:
    
    <h1 class="uppercase text-center text-3xl font-bold">Etiqueta: {{$tag->name}}</h1>


En nuestro 'card-post.blade.php' agregamos:

    @foreach ($post->tags as $tag)
          <a href="{{route('posts.tag',$tag)}}" class=...>
             {{$tag->name}}
          </a>
    @endforeach

Hacemos algo similar en el '@foreach ($post->tags as $tag)' de 'index.blade.php'

De esta forma, tenemos links a la ruta recién definida en todos los lugares pertinentes.






10. Cómo integrar AdminLTE 3 en tu proyecto de Laravel

Debemos primero, evitar que al hacer Login nos dirija automáticamente al Dashboard.
Voy a 'app/Providers/RouteServiceProvider.php' y cambio:

    public const HOME = '/dashboard';

Por
    public const HOME = '/';


Necesito generar una URL 'admin':
Creamos un archivo 'admin.php' en nuestro directorio '<root>/routes'

Lo agregamos en RouteServiceProvider, incluyendo el Middleware 'auth' ademas del 'web'.
Ademas, agregamos el prefijo 'admin':

    Route::middleware('web','auth')
        ->prefix('admin')
        ->group(base_path('routes/admin.php'));


Damos en la terminal:

    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:controller Admin\\HomeController
       INFO  Controller [app/Http/Controllers/Admin/HomeController.php] created successfully.


En 'admin.php' definimos:

    use Illuminate\Support\Facades\Route;
    use App\Http\Controllers\Admin\HomeController;

    Route::get('', [HomeController::class,'index'])->name('admin.home');

En HomeController:

    public function index(){
      return view('admin.index');
    }

Creamos archivo 'resources/views/admin/index.blade.php' (por ahora vacio o con un texto muy sencillo de prueba)


Procedemos a instalar AdminLTE:

Docs:
https://github.com/jeroennoten/Laravel-AdminLTE/wiki (Link en comentario del video) -> Vamos a 'Installation'
https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Installation


    -Paso 1
    Damos en la terminal:
        leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ composer require jeroennoten/laravel-adminlte

    -Paso 2
    Lo salteamos ya que esto es necesario para implementar los sistemas de autentificación, que ya nos vinieron instalados con JetStream

    -Paso 3
    Damos en la terminal:
        leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan adminlte:install

    ¡OJO! Los pasos 2 y 3 están invertidos en la útlima versión de la página, respecto a la mostrada en el video tutorial


Vamos ahora, a la doc de 'Usage':
https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Usage

Con añadir en nuestro nuevo 'index.blade.php' la línea:
'@extends('adminlte::page')'

Si fue correctamente instalado, ya veremos una correcta visualización de la interfaz de AdminLTE.

Mas abajo en esa seccion de la doc, vemos un código mas completo con el cual probar mejor la interfaz de este nuevo plugin.

Ver imágen 'admin_lte.png'





BACKEND DE LA APLICACION


11. Crud de categorías

En 'config/adminlte.php' podemos personalizar lo que se muestra en la plantilla por defecto de AdminLTE

En el Profile menú, agregamos un item "Dashboard" que nos direccione a "route('admin.home')"

Edito el 'config/adminlte.php' de acuerdo a los valores indicados en el video.

Me voy a 'Menú items'. Tengo: 

    'menu' => [
        ...
    ],

Allí defino los elementos del sidebar de AdminLTE, de acuerdo al código que copiamos del video

https://fontawesome.com/v5/icons/tachometer-alt?f=classic&s=solid -> Ícono de ejemplo

Damos:
    php artisan make:controller Admin\\CategoryController -r


En 'admin.php' (enrutamiento):

    use App\Http\Controllers\Admin\CategoryController;
    Route::resource('categories', CategoryController::class)->names('admin.categories');


El video luego muestra como definir los 7 métodos, rutas y vistas de Resource (ya lo tenemos hecho)

Creamos las vistas necesarias de acuerdo a la ubicación indeicada en el video, y las llenamos con el código allí descripto.

ToDo -> Hacerlo con AdminLTE como muestra en el video

Usamos Laravel Collective: Optimiza en gran medida la utilización y definición de forms en Laravel, sobre todo en las relaciones

Instalación de Laravel Collective:
https://laravelcollective.com/docs/6.x/html

Terminal:
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ composer require laravelcollective/html

https://leocaseiro.com.br/jquery-plugin-string-to-slug/

Bajo 'jquery.stringToSlug.js 1.3' y copio la carpeta 'jQuery-Plugin-stringToSlug-1.3' en '.../blog/public/vendor'

Tomar en cuenta creación de formularios con Laravel Collective
ToDo: Dejar para despues copiar el código del video (no es tan relevante para nosotros por ahora)



12. Crud de etiquetas
Simil clase anterior


Damos:
    php artisan make:controller Admin\\TagController -r


En 'admin.php' (enrutamiento):

    use App\Http\Controllers\Admin\TagController;
    Route::resource('tags', TagController::class)->names('admin.tags');


13. Cómo mostrar mis posts creados

IMPORTANTE: Tomar en cuenta el siguiente tutorial de JQuery DataTables:
https://www.youtube.com/watch?v=xyGriTTRo_o&list=PLZ2ovOgdI-kVmp2KynrUyosIazUhGK-6n

Sirve para agilizar y formatear mejor los resultados de las queries


En este caso, utilizaremos LiveWire (mas allá de que en este ejemplo esté incorporado en AdminLTE)


Es para tomar en cuenta, en esta clase, para utilizar LiveWire correctamente, los archivos y directorios:

    routes/admin.php (Enrutamiento PostController)
    app/Http/Controllers/Admin/PostController.php
    app/Livewire/Admin/PostsIndex.php
    resources/views/livewire/admin/posts-index.blade.php
    resources/views/admin/posts (Directorio, sobre todo 'index.blade.php' que llama a componente LiveWire)
    config/adminlte.php (Ultima línea sobre todo)

Para probar resultados:
    http://localhost:8000/admin/posts




14. Formulario para crear nuevo post

En el @section('content') de 'create.blade.php' implemento el form (en el video, lo hace con Laravel Collective)

Recomendacion para forms con texto enriquecido: CKEditor

    https://ckeditor.com/ckeditor-5/download/
    <script src="https://cdn.ckeditor.com/ckeditor5/40.0.0/classic/ckeditor.js"></script> (Donde corresponda)

En el @section('js'),  implemento los scripts necesarios tanto para trabajar con CKEditor como para generar el campo slug de acuerdo a lo recomendado por el video


https://ckeditor.com/docs/ckeditor5/latest/installation/getting-started/quick-start.html

15. Cómo crear un nuevo post

En la migration de 'posts', debo hacer 'nullable' los campos 'extract' y 'body'

Damos:
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:request StorePostRequest
       INFO  Request [app/Http/Requests/StorePostRequest.php] created successfully.  

Ver reglas de validacion condicionales en ese archivo

¡OJO! Acá nos da una pista de como hacer asignacion masiva en tablas relacionadas

Rutas relevantes:
    app/Http/Requests/StorePostRequest.php
    app/Http/Controllers/Admin/PostController.php (método store)

ToDo: Completar form en vista 'edit.blade.php' de acuerdo al video






16. Cómo subir imágenes al servidor

Debemos agregar @if($post->image) en las vistas Blade, por si tenemos algún post que no hayamos cargado imágens (tendríamos error al hacer $post->image->url)



IMPORTANTE:

Si tengo:

    <div class="text-base text-gray-700">
        {{$post->extract}}
    </div>

Y en $post->extract tengo: '<p>Hola</p>'

Laravel me muestra en el navegador '<p>Hola</p>'

Para que me lo renderize como HTML debo hacer:

    <div class="text-base text-gray-700">
        {!!$post->extract!!}
    </div>


Muestra como realizar asignacion masiva incluso de imágenes-> Revisar:

    app/Http/Requests/StorePostRequest.php
    app/Http/Controllers/Admin/PostController.php (método store)

Asegurarse de habilitar asignación masiva (con $fillable o $guarded) en todos los modelos relevantes


ToDo: Definir código explicado en clases de 14 a 16




17. Cómo actualizar un post

Puedo usar partials:

Defino 'form.blade.php'. Desde 'edit.create.php' puedo hacer:

{!! Form::model($post, ['route'=>['admin.posts.update',$post] , 'autocomplete' =>'off' , 'files' =>true, 'method'=>'put']) !!}

    {!! Form::hidden('user_id', auth()->user()->id) !!}

    @include('admin.posts.partials.form')

    {!! Form::submit('Actualizar Post', ['class'=>'btn btn-primary']) !!}

{!! Form::close() !!}

Algo similar desde 'create.blade.php'


Renombrar StorePostRequest por PostRequest
Ver actualizacion de 'update' en PostController




18. Observers en laravel ¿Qué son y para que sirven?


Metodo 'attach' -> Siempre añade registros a la tabla pivote, no nos sirve para el update

En 'update', reemplazamos 'attach' por 'sync'


Observer: Clases que nos permiten agrupar eventos relacionados a un determinado modelo.
Dichos eventos se van a ejecutar cada vez que realizemos alguna acción relacionada a ese modelo (como crear, actualizar o eliminar un registro).

Damos:
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:observer PostObserver --model=Post
       INFO  Observer [app/Observers/PostObserver.php] created successfully.

La clase creada en ese archivo tiene métodos (inicialmente vacíos) asociados a cada evento

Método 'delete': Se ejecuta DESPUES que se haya eliminado el post
Método 'deleting': Se ejecuta ANTES que se haya eliminado el post


Debemos registrar el Observer en EventServiceProvider:
    
    public function boot(): void
    {
        Post::observe(PostObserver::class);
    }



19. Policies en laravel ¿Qué son y para que sirven? 

Ya estudiado, nada nuevo

Damos:
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:policy PostPolicy
   INFO  Policy [app/Policies/PostPolicy.php] created successfully.  

Ver código en:
    'app/Policies/PostPolicy.php'
    'app/Http/Controllers/PostController.php'
    'app/Http/Controllers/Admin/PostController.php'

Implementamos $this->authorize en los métodos de los controladores (no hizo falta registrar manualmente la Policy en los ServiceProviders)



ROLES Y PERMISOS


20. Instalar Laravel Permission

Siempre que queremos implementar un sistema de roles y permisos, necesitamos tener 6 tablas creadas en nuestra base de datos, con sus relaciones correspondientes.
Ver 'roles_permisos.png'

Permisos: Acciones que queremos limitar dentro de nuestra app.

Un usuario puede tener varios permisos, y un permiso puede estar presente en varios usuarios-> Relacion n a n

Spatie la implementa automáticamente de forma polimorfica


Roles: Agrupan cierta cantidad de permisos.
Ej: Rol 'profesor': Le asignamos 50 permisos.
Si a un usuario le asignamos el rol de 'profesor', ese usuario tendrá tales 50 permisos

También se nos permite dar un permiso especifico a un determinado usuario (por fuera de su rol)

Vamos a:
https://spatie.be/docs/laravel-permission/v5/installation-laravel

Damos:
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$  composer require spatie/laravel-permission

Luego, para publicar las migrations:

    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"
       INFO  Publishing assets.  
      Copying file [vendor/spatie/laravel-permission/config/permission.php] to [config/permission.php]  DONE
      Copying file [vendor/spatie/laravel-permission/database/migrations/create_permission_tables.php.stub] to [database/migrations/2023_10_18_101304_create_permission_tables.php]  DONE

Luego 'php artisan migrate'

Spatie automáticamente me crea los modelos necesarios con sus respectivas relaciones.

Tenemos los modelos Role y Permission en:
    'vendor/spatie/laravel-permission/src/Models/Permission.php'
    'vendor/spatie/laravel-permission/src/Models/Role.php'

El trait relevante en:
    'vendor/spatie/laravel-permission/src/Traits/HasRoles.php'

Tenemos el archivo de configuración pertinente en:
    'config/permission.php'


Miramos: 'https://spatie.be/docs/laravel-permission/v5/basic-usage/basic-usage'

Solo debemos editar el modelo 'User':

    ...
    use Spatie\Permission\Traits\HasRoles;

    class User extends Authenticatable
    {
        ...
        use HasRoles;
        ...




21. Cómo crear roles y permisos

Podemos generar un permiso para que un usuario pueda o bien ver, crear, editar o eliminar, ya sea una categoría, una etiqueta, o un post, de acuerdo a las acciones posibles que definimos hasta ahora para dichos modelos.

Para asignar permisos a cada rol definido, debo colocar registros en la tabla intermedia 'role_has_permissions' (con dos columnas 'permission_id' y 'role_id').

Podríamos utilizar:

    $role1->permissions()->attach([1,2...]);

Pero nos conviene usar:

    $role->givePermissionTo($permission);
    $permission->assignRole($role);

(Se usan de forma inversa o complementaria, pero su efecto es el mismo)

Para asignar multiples permisos/roles en una sola línea (recibiendo array de instancias de permisos o bien roles):

    $role->syncPermissions($permissions);
    $permission->syncRoles($roles);


En nuestro DatabaseSeeder nos puede quedar:

      use Spatie\Permission\Models\Role;
      use Spatie\Permission\Models\Permission;
        
        ...

      $role1=Role::create(['name'=>'Admin']);
      $role2=Role::create(['name'=>'Blogger']);

      Permission::create(['name'=>'admin.home'])->syncRoles([$role1,$role2]);

      Permission::create(['name'=>'admin.categories.index'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.categories.create'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.categories.edit'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.categories.destroy'])->syncRoles([$role1,$role2]);

      Permission::create(['name'=>'admin.tags.index'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.tags.create'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.tags.edit'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.tags.destroy'])->syncRoles([$role1,$role2]);

      Permission::create(['name'=>'admin.posts.index'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.posts.create'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.posts.edit'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.posts.destroy'])->syncRoles([$role1,$role2]);

      User::create([
         'name' => 'Leandro Caplan',
         'email' => 'leandrocaplan@gmail.com',
         'password' => bcrypt('q1w2e3r4')
      ])->assignRole('Admin');

      User::create([
         'name' => 'Gabriela Rositto',
         'email' => 'rosittog_86@hotmail.com',
         'password' => bcrypt('qwertyui')
      ])->assignRole('Blogger');

      User::create([
         'name' => 'Juan Perez',
         'email' => 'juanperez@hotmail.com',
         'password' => bcrypt('password')
      ]); //No le asigno ningún rol
    ...







22. Asignar rol a usuarios

Damos:
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:controller Admin\\UserController -r
       INFO  Controller [app/Http/Controllers/Admin/UserController.php] created successfully.


En nuestro 'routes/admin.php' agregamos:

    use App\Http\Controllers\Admin\UserController;
    Route::resource('users', UserController::class)->names('admin.users');


Creamos archivos relevantes en 'views\admin\users' (index,edit,create,show).

Añadimos elemento de sidebar en 'config/adminlte.php' (Justo abajo de Dashboard/Panel principal, y arriba de la sección 'ADMINISTRADOR')


Damos:
    leandro@leandro-Lenovo-B50-10:/var/www/html/blog$ php artisan make:livewire Admin\\UsersIndex
     COMPONENT CREATED  🤙

    CLASS: app/Livewire/Admin/UsersIndex.php
    VIEW:  resources/views/livewire/admin/users-index.blade.php

En estos dos archivos generados, copio el código del video


Modifico mi DatabaseSeeder (solo quiero que los admin puedan hacer crud de users, cartegorias y etiquetas):

  $role1=Role::create(['name'=>'Admin']);
  $role2=Role::create(['name'=>'Blogger']);

  Permission::create(['name'=>'admin.home'])->syncRoles([$role1,$role2]);

  Permission::create(['name'=>'admin.users.index'])->syncRoles([$role1]);
  Permission::create(['name'=>'admin.users.edit'])->syncRoles([$role1]);
  Permission::create(['name'=>'admin.users.destroy'])->syncRoles([$role1]);

  Permission::create(['name'=>'admin.categories.index'])->syncRoles([$role1]);
  Permission::create(['name'=>'admin.categories.create'])->syncRoles([$role1]);
  Permission::create(['name'=>'admin.categories.edit'])->syncRoles([$role1]);
  Permission::create(['name'=>'admin.categories.destroy'])->syncRoles([$role1]);

  Permission::create(['name'=>'admin.tags.index'])->syncRoles([$role1]);
  Permission::create(['name'=>'admin.tags.create'])->syncRoles([$role1]);
  Permission::create(['name'=>'admin.tags.edit'])->syncRoles([$role1]);
  Permission::create(['name'=>'admin.tags.destroy'])->syncRoles([$role1]);

  Permission::create(['name'=>'admin.posts.index'])->syncRoles([$role1,$role2]);
  Permission::create(['name'=>'admin.posts.create'])->syncRoles([$role1,$role2]);
  Permission::create(['name'=>'admin.posts.edit'])->syncRoles([$role1,$role2]);
  Permission::create(['name'=>'admin.posts.destroy'])->syncRoles([$role1,$role2]);


En mi router hago:
    Route::resource('users', UserController::class)->only(['index','edit','update'])->names('admin.users');

(Solo utlizaré tres rutas de las siete predeterminadas con resource)

En UserController podemos eliminar los métodos que sobran





23. Cómo ocultar botones según el rol

Vamos a 'resources/views/livewire/navigation.blade.php', donde tenemos:
    <a href="{{route('admin.home')}}" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-0">Dashboard</a>

Lo encierro en un:
    @can('admin.home')...@endcan

Si entro con un usuario que no tenga asignado ningún rol -> No veo el item 'Dashboard' en el menú --> @can funciona para los permisos igual que para Gates o métodos de Policy.


Para renderizar condicionalmente el sidebar de AdminLTE -> Vamos a 'config/adminlte.php':

A cada elemento del menú configurado en un array, le agregamos un atributo 'can':

        [
            'text'        => 'Panel principal', //Teníamos originalmente 'Dashboard'
            //'url'         => 'admin', //Teníamos originalmente 'admin/pages'
            'route' => 'admin.home', //Equivalente al campo 'url'
            'icon' => 'fas fa-tachometer-alt fa-fw', //Teníamos originalmente 'far fa-fw fa-file'
            'can' => 'admin.home'

        ],



Renderizamos luego, condicionalmente las vistas de acuerdo a los permissions que tengamos para un usuario con determinado rol, en nuestras vistas Blade






24. Cómo proteger rutas de acuerdo al rol


Para evitar que se acceda a la URL manualmente, implementamos middleware en la definicion de las rutas-> Utilizamos middleware 'can'.

En 'routes/admin.php' puedo hacer:
    Route::get('', [HomeController::class,'index'])->middleware('can:admin.home')->name('admin.home');

Nunca utilizamos la vista show en categories, tags, ni posts, asi que hacemos:
    Route::resource('categories', CategoryController::class)->except('show')->names('admin.categories');

En el resto de las rutas allí definidas, no puedo hacer lo mismo: debo implementar el middleware en los Controller.

Defino una función __construct en cada Controller:

   public function __construct()
   {
      $this->middleware('can:admin.posts.index')->only('index');
      $this->middleware('can:admin.posts.create')->only('create','store');
      $this->middleware('can:admin.posts.edit')->only('edit','update');
      $this->middleware('can:admin.posts.destroy')->only('destroy');
   }




25. Crear un crud para los roles.

Añado rutas, controladores y vistas como en los modelos anteriores.

Voy a 'database/migrations/2023_10_18_101304_create_permission_tables.php' y agrego:

        Schema::create($tableNames['permissions'], function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->string('name');      
            $table->string('description'); //ESTA LINEA
            $table->string('guard_name');
            $table->timestamps();

            $table->unique(['name', 'guard_name']);
        });


Actualizamos nuestro DatabaseSeeder de la forma:

    Permission::create(['name'=>'admin.home','description'=>'Ver el Dashboard'])->syncRoles([$role1,$role2]);
    Permission::create(['name'=>'admin.users.index','description'=>'Ver listado de usuarios'])->syncRoles([$role1]);
    ...

Damos 'php migrate:fresh --seed'






26. Cómo funciona la caché en Laravel


Una consulta SQL puede ir acompañada de una serie de filtros que puede demorar bastante la base de datos en el server para darnos la respuesta que necesitamos.
Mientras mas compleja sea la consulta, mas demora la respuesta del servidor.

La caché de Laravel intenta solventar en parte este problema: Ejecuto la consulta una vez, y almacenamos su resultado en caché, o bien en un archivo o bien en memoria.

Verficamos primero si la consulta existe en caché. De ser así, obtenemos el resultado desde la caché. Ver 'cache.png'.


Vamos a 'config\cache.php'. Tenemos:


    'default' => env('CACHE_DRIVER', 'file'), //Busca la forma de almacenamiento de caché, si es posible, desde el .env, o sinó en este caso 'file' (por defecto)

        /*
        |--------------------------------------------------------------------------
        | Cache Stores
        |--------------------------------------------------------------------------
        |
        | Here you may define all of the cache "stores" for your application as
        | well as their drivers. You may even define multiple stores for the
        | same cache driver to group types of items stored in your caches.
        |
        | Supported drivers: "apc", "array", "database", "file",
        |         "memcached", "redis", "dynamodb", "octane", "null" //Todas las formas de almacenamiento en caché que podremos utilizar
        |
        */


En nuestro .env tenemos (por defecto):

    'CACHE_DRIVER=file'


Esto significa que la caché se almacena en archivos.



Vamos a 'app/Http/Controllers/PostController.php':
Importamos:
'use Illuminate\Support\Facades\Cache;'

Método 'index' --> Reemplazamos:

    '$posts=Post::where('status',2)->latest('id')->paginate(8);'

Por:

      if (Cache::has('posts')) {
         $posts=Cache::get('posts');
      } else {
         $posts=Post::where('status',2)->latest('id')->paginate(8);
         Cache::put('posts',$posts);
      }

Si existe la consulta que fue previamente devuelta, almacenada en caché, obtenemos su resultado desde allí.
Si no existe, la hacemos y guardamos su resultado en caché para las veces subsiguientes que la querramos realizar.

Por el momento, siempre nos devuelve la información de la primera página (debido a que usamos 'paginate').
Para solucionarlo, me guardo una variable de caché diferente cuyo nombre estará determinado por la página que pasemos mediante URL:

Ej:
    http://localhost:8000/
    http://localhost:8000/?page=1
    http://localhost:8000/?page=2
    http://localhost:8000/?page=3
    ..

Hacemos entonces:

      if (request()->page) {
         $key='posts'.request()->page; //Tendremos 'posts1','posts2', etc...
      } else {
         $key='posts';
      }
      
      if (Cache::has($key)) {
         $posts=Cache::get($key);
      } else {
         $posts=Post::where('status',2)->latest('id')->paginate(8);
         Cache::put($key,$posts);
      }



27. Cómo actualizar datos de caché en Laravel


Si creamos un nuevo post, y traemos la consulta desde caché sin actualizarla, en dicha consulta no veremos reflejado el nuevo post.
Al hacerlo debemos eliminar TODAS las variables de caché (ya que previamente creamos una por cada página, no una sola).

Vamos a 'app/Http/Controllers/Admin/PostController.php'.

Importamos la facade 'Cache' como en el Controller anterior.

Para eliminar una sola variable de caché, usamos el método 'forget' de la forma:

    'Cache::forget('posts');'

Para eliminar TODAS las variables de caché hacemos:

    'Cache::flush();'

En los métodos 'store', 'update' y 'destroy', añadimos esa última línea antes del 'return'.
En métodos 'store', 'update' y 'destroy'

Metodo 'store':

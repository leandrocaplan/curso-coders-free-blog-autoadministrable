<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Post;

class PostPolicy
{
   use HandlesAuthorization;
    /**
     * Create a new policy instance.
     */
    /*
    public function __construct()
    {
        //
    }
*/
    public function author(User $user, Post $post){
      return $user->id==$post->user_id;
    }

    public function published(?User $user, Post $post){
      return $post->status==2;
    }
}

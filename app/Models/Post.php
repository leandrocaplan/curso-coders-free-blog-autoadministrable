<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Image;

class Post extends Model
{
    use HasFactory;

    protected $guarded=['id','created_at','updated_at']; //¡OJO! Si dej el array vacío, al hacer create($request->all()), me intenta insertar valores en la columna inexistente 'tags'
     //Relaciones 1 a n inversa
     public function user(){
          return $this->belongsTo(User::class);
     }

     public function category(){
          return $this->belongsTo(Category::class);
     }

     //Relacion n a n
     public function tags(){
         return $this->belongsToMany(Tag::class);
     }

     //relacion 1 a 1 polimorfica
     public function image(){
         return $this->morphOne(Image::class,'imageable');
     }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
   public function index(){

      if (request()->page) {
         $key='posts'.request()->page;
      } else {
         $key='posts';
      }
      
      if (Cache::has($key)) {
         $posts=Cache::get($key);
      } else {
         $posts=Post::where('status',2)->latest('id')->paginate(8);
         Cache::put($key,$posts);
      }
      

      return view('posts.index',compact('posts'));
   }

   public function show(Post $post){
      $this->authorize('published',$post);
      $similares=Post::where('category_id',$post->category_id)
         ->where('status',2)
         ->where('id','!=',$post->id)
         ->latest('id')
         ->take(4)
         ->get();
      return view('posts.show',compact('post','similares'));

      //return get_class($post->tags->first()->pivot); //Illuminate\Database\Eloquent\Relations\Pivot
      //return $post->tags;
      /*
      [
         {
            "id": 3,
            "name": "nemo",
            "slug": "nemo",
            "color": "pink",
            "created_at": "2023-10-17T21:00:14.000000Z",
            "updated_at": "2023-10-17T21:00:14.000000Z",
            "pivot": {
               "post_id": 98,
               "tag_id": 3
            }
         },
         {
            "id": 7,
            "name": "aspernatur",
            "slug": "aspernatur",
            "color": "purple",
            "created_at": "2023-10-17T21:00:14.000000Z",
            "updated_at": "2023-10-17T21:00:14.000000Z",
            "pivot": {
               "post_id": 98,
               "tag_id": 7
            }
         }
      ]
      */
      
   }

   public function category(Category $category){
      $posts=Post::where('category_id',$category->id)
         ->where('status',2)
         ->latest('id')
         ->paginate(4);

      return view('posts.category',compact('posts','category'));
   }

   public function tag(Tag $tag){
      $posts=$tag->posts()
         ->where('status',2)
         ->latest('id')
         ->paginate(4);

      return view('posts.tag',compact('posts','tag'));
   }
}

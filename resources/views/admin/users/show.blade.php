@extends('adminlte::page')

@section('title', 'Blog de ejemplo')

@section('content_header')
    <h1>Detalles del post</h1>
@stop

@section('content')
    <p>Bienvenidos a este hermoso panel administrativo.</p>
@stop

@section('css')
   {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script> console.log('¡Hola!'); </script>
@stop
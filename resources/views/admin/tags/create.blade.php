@extends('adminlte::page')

@section('title', 'Blog de ejemplo')

@section('content_header')
<h1>Crear nueva etiqueta</h1>
@stop

@section('content')

   @if(session('info'))
         <div class="alert alert-success">
            <strong>{{session('info')}}</strong>
         </div>
   @endif
<div class="card">
   <div class="card-body">
      {!! Form::open(['route' => 'admin.tags.store']) !!}

      @include('admin.tags.partials.form')

      {!! Form::submit('Crear categoría', ['class'=>'btn btn-primary']) !!}
      {!! Form::close(['route' => 'admin.tags.tags']) !!}
   </div>


</div>
@stop

@section('css')
{{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
<script>
   console.log('¡Hola!');
</script>

<script src="{{asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js')}}">
   
</script>
<script>
   $(document).ready(function() {
      $("#name").stringToSlug({
         setEvents: 'keyup keydown blur',
         getPut: '#slug',
         space: '-'
      });
   });
</script>

@stop
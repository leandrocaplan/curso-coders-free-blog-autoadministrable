@php
   error_log("Renderizo vista 1");
@endphp

<x-app-layout>
   
   @php
      error_log("Renderizo vista 2");
   @endphp
   <div class="container py-8">
      <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
         @foreach ($posts as $post)
         
            <article 
               class="w-full h-80 bg-cover bg-center
                     @if($loop->first)
                        md:col-span-2
                     @endif"
               style="background-image: url(
                  @if($post->image)
                     {{Storage::url($post->image->url)}}
                  @else
                     https://cdn.pixabay.com/photo/2023/10/16/10/51/fox-8318961_1280.png
                  @endif
                  )">
               

               <div class="w-full h-full px-8 flex flex-col justify-center">
              
                  <div>
                  @foreach ($post->tags as $tag)   
                        <a href="{{route('posts.tag',$tag)}}" class="inline-block px-3 h-6 bg-{{$tag->color}}-600 text-white rounded-full">
                           {{$tag->name}}
                        </a>

                        <script>
                           //Esta funcionalidad presenta un bug: no siempre se cargan los colores de fondo asignados a cada tag (dependiendo del color)
                           //Al harcodear el valor, siempre funcióna, pero no al hacerlo de forma dinámica mediande llaves de Blade
                           console.log("bg-{{$tag->color}}-600");
                           if("bg-{{$tag->color}}-600"==="bg-{{$tag->color}}-600"){
                              console.log("True");
                           }

                        </script>
                     @endforeach
                  </div>   
                             
                  <h1 class="text-4xl text-white leading-8 font-bold">
                     <a href="{{route('posts.show',$post)}}">
                        {{$post->name}}
                     </a>
                  </h1>
               </div>
            </article>
         @endforeach
      </div>

      <div class="mt-4">
         {{$posts->links()}}
      </div>
   </div>
</x-app-layout>
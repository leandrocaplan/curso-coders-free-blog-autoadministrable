<article class="mb-8 bg-white shadow-lg rounded-lg overflow-hidden">
   <img class="w-full h-72 object-cover object-center" src="
   @if($post->image)
      {{Storage::url($post->image->url)}}
   @else
      https://cdn.pixabay.com/photo/2023/10/16/10/51/fox-8318961_1280.png
   @endif
   ">

   <div class="px-6 py-4">
      <h1 class="text-xl font-bold mb-2">
         <a href="{{route('posts.show',$post)}}">{{$post->name}}</a>
      </h1>

      <div class="text-base text-gray-700">
         {!!$post->extract!!}
      </div>
   </div>

   <div class="px-6 pt-4 pb-2">
      @foreach ($post->tags as $tag)
      <a href="{{route('posts.tag',$tag)}}" class="inline-block px-3 py-1 bg-gray-300 border-[1px] border-[#5533DD]  text-sm font-black rounded-full text-gray-800 mr-2">
         {{$tag->name}}
      </a>
      @endforeach
   </div>
</article>
<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Category;
use App\Models\User;
use App\Models\Image;
use App\Models\Tag;
use App\Models\Post;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
   /**
    * Seed the application's database.
    */
   public function run(): void
   {


      //En nuestro .env debemos tener: 'FILESYSTEM_DISK=public'
      //Sinó, tendremos un error al ejecutar los métodos de Storage
      Storage::deleteDirectory('posts');
      Storage::makeDirectory('posts');

      $role1=Role::create(['name'=>'Admin']);
      $role2=Role::create(['name'=>'Blogger']);

      Permission::create(['name'=>'admin.home','description'=>'Ver el Dashboard'])->syncRoles([$role1,$role2]);

      Permission::create(['name'=>'admin.users.index','description'=>'Ver listado de usuarios'])->syncRoles([$role1]);
      Permission::create(['name'=>'admin.users.edit','description'=>'Asignar un rol'])->syncRoles([$role1]);
      Permission::create(['name'=>'admin.users.destroy','description'=>'Eliminar usuario'])->syncRoles([$role1]);

      Permission::create(['name'=>'admin.categories.index','description'=>'Ver listado de categorías'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.categories.create','description'=>'Crear categorías'])->syncRoles([$role1]);
      Permission::create(['name'=>'admin.categories.edit','description'=>'Editar categorías'])->syncRoles([$role1]);
      Permission::create(['name'=>'admin.categories.destroy','description'=>'Eliminar categorías'])->syncRoles([$role1]);

      Permission::create(['name'=>'admin.tags.index','description'=>'Ver listado de etiquetas'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.tags.create','description'=>'Crear etiquetas'])->syncRoles([$role1]);
      Permission::create(['name'=>'admin.tags.edit','description'=>'Editar etiquetas'])->syncRoles([$role1]);
      Permission::create(['name'=>'admin.tags.destroy','description'=>'Eliminar etiquetas'])->syncRoles([$role1]);

      Permission::create(['name'=>'admin.posts.index','description'=>'Ver listado de posts'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.posts.create','description'=>'Crear post'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.posts.edit','description'=>'Editar post'])->syncRoles([$role1,$role2]);
      Permission::create(['name'=>'admin.posts.destroy','description'=>'Eliminar post'])->syncRoles([$role1,$role2]);

      User::create([
         'name' => 'Leandro Caplan',
         'email' => 'leandrocaplan@gmail.com',
         'password' => bcrypt('q1w2e3r4')
      ])->assignRole('Admin'); //Puedo hacer: assignRole($role1);
      

      User::create([
         'name' => 'Gabriela Rositto',
         'email' => 'rosittog_86@hotmail.com',
         'password' => bcrypt('qwertyui')
      ])->assignRole('Blogger'); //Puedo hacer: assignRole($role2);

      User::create([
         'name' => 'Juan Perez',
         'email' => 'juanperez@hotmail.com',
         'password' => bcrypt('password')
      ]);

      User::factory(98)->create();

      Category::factory(4)->create();
      Tag::factory(8)->create();

      $posts = Post::factory(100)->create();

      foreach ($posts as $post) {

         Image::factory(1)->create([
            'imageable_id' => $post->id,
            'imageable_type' => Post::class
         ]);

         $post->tags()->attach([
            rand(1, 4),
            rand(5, 8),
         ]);
      }
   }
}
